#!/bin/bash
set -euxo pipefail

# Print some information about the machine
df -h
free -h

# Tests for osbuild-composer on RHEL 9 fail for multiple reasons,
# see: https://github.com/osbuild/osbuild-composer/issues?q=is%3Aopen+is%3Aissue+label%3Arhel9

# Let's run only image tests for now.
/usr/libexec/osbuild-composer-test/osbuild-image-tests \
    -disable-local-boot -fail-local-boot=false \
    -skip-selinux-ctx-check \
    /usr/share/tests/osbuild-composer/manifests/rhel_90-*
